# syntax=docker/dockerfile:1
FROM python:3
ENV PYTHONUNBUFFERED=1
WORKDIR /django

# Add wait script to image
ADD https://github.com/ufoscout/docker-compose-wait/releases/download/2.9.0/wait /wait
RUN chmod +x /wait

# Install requirements
COPY django/requirements.txt /django/
RUN pip install -r requirements.txt

# Add application code to image
COPY django/ /django/
