from django.urls import path
from . import views

app_name = 'cf'
urlpatterns = [
	path('', views.dashboard, name='dashboard'),
	path('dashboard/', views.dashboard, name='dashboard'),

	path('controlframework', views.ControlFrameworkListView.as_view(), name='controlframework'),

	path('policies', views.DomainListView.as_view(), name='policies'),
	
	path('policies/domain/<int:pk>', views.DomainView.as_view(), name='policies/domain'),
	path('policies/domain/add/', views.DomainCreate.as_view(), name='policies/domain/add'),
	path('policies/domain/<int:pk>/update/', views.DomainUpdate.as_view(), name='policies/domain/update'),
	path('policies/domain/<int:pk>/delete/', views.DomainDelete.as_view(), name='policies/domain/delete'),

	path('policies/genericpolicy/<int:pk>', views.GenericPolicyView.as_view(), name='policies/genericpolicy'),
	path('policies/genericpolicy/add/domain/<int:domain_id>', views.GenericPolicyCreate.as_view(), name='policies/genericpolicy/add/domain'),
	path('policies/genericpolicy/<int:pk>/update', views.GenericPolicyUpdate.as_view(), name='policies/genericpolicy/update'),
	path('policies/genericpolicy/<int:pk>/delete', views.GenericPolicyDelete.as_view(), name='policies/genericpolicy/delete'),

	path('policies/policy/<int:pk>', views.PolicyView.as_view(), name='policies/policy'),
	path('policies/policy/add/genericpolicy/<int:genericpolicy_id>', views.PolicyCreate.as_view(), name='policies/policy/add/genericpolicy'),
	path('policies/policy/update/<int:pk>', views.PolicyUpdate.as_view(), name='policies/policy/update'),
	path('policies/policy/delete/<int:pk>', views.PolicyDelete.as_view(), name='policies/policy/delete'),

	path('controls/', views.GenericControlListView.as_view(),name='controls'),
	
	path('controls/genericcontrol/<int:pk>/', views.GenericControlView.as_view(),name='controls/genericcontrol'),
	path('controls/genericcontrol/add/', views.GenericControlCreate.as_view(),name='controls/genericcontrol/add'),
	path('controls/genericcontrol/<int:pk>/update/', views.GenericControlUpdate.as_view(),name='controls/genericcontrol/update'),
	path('controls/genericcontrol/<int:pk>/delete/', views.GenericControlDelete.as_view(),name='controls/genericcontrol/delete'),
	
	path('controls/control/<int:pk>/', views.ControlView.as_view(),name='controls/control'),
	path('controls/control/add/genericcontrol/<int:genericcontrol_id>/', views.ControlCreate.as_view(),name='controls/control/add/genericcontrol'),
	path('controls/control/<int:pk>/update/', views.ControlUpdate.as_view(),name='controls/control/update'),
	path('controls/control/<int:pk>/delete/', views.ControlDelete.as_view(),name='controls/control/delete'),


	path('objects/', views.GenericObjectListView.as_view(),name='objects'),
	
	path('objects/genericobject/<int:pk>/', views.GenericObjectView.as_view(),name='objects/genericobject'),
	path('objects/genericobject/add/', views.GenericObjectCreate.as_view(),name='objects/genericobject/add'),
	path('objects/genericobject/<int:pk>/update/', views.GenericObjectUpdate.as_view(),name='objects/genericobject/update'),
	path('objects/genericobject/<int:pk>/delete/', views.GenericObjectDelete.as_view(),name='objects/genericobject/delete'),
	
	path('objects/object/<int:pk>/', views.ObjectView.as_view(),name='objects/object'),
	path('objects/object/add/genericobject/<int:genericobject_id>/', views.ObjectCreate.as_view(),name='objects/object/add/genericobject'),
	path('objects/object/<int:pk>/update/', views.ObjectUpdate.as_view(),name='objects/object/update'),
	path('objects/object/<int:pk>/delete/', views.ObjectDelete.as_view(),name='objects/object/delete'),
]