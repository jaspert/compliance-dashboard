from django.contrib.auth.mixins import PermissionRequiredMixin
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse, reverse_lazy
from django.views.generic import ListView, DetailView

from cdash_project.views import CreateView, UpdateView, DeleteView
from ..models import Domain, GenericPolicy, Policy
from ..forms import DomainForm, GenericPolicyForm, PolicyForm
from .. import navigation

## Domain

class DomainListView(PermissionRequiredMixin, ListView):
	permission_required = 'controlframework.view_domain'
	model = Domain
	template_name = 'controlframework/policies/policies.html'
	nav_item_active = navigation.app.nav_items['policies']

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context['title'] = 'Policies'
		context['nav_item_active'] = self.nav_item_active
		return context

class DomainView(PermissionRequiredMixin, DetailView):
	permission_required = 'controlframework.view_domain'
	model = Domain
	template_name = 'controlframework/policies/domain.html'
	nav_item_active = navigation.app.nav_items['policies']

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context['title'] = 'Domain'
		context['nav_item_active'] = self.nav_item_active
		return context

class DomainCreate(PermissionRequiredMixin, CreateView):
	permission_required = 'controlframework.add_domain'
	model = Domain
	form_class = DomainForm
	template_name = 'controlframework/form.html'
	nav_item_active = navigation.app.nav_items['policies']

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context['title'] = 'Create Domain'
		context['nav_item_active'] = self.nav_item_active
		return context

	def post(self, request, *args, **kwargs):
		if "cancel" in request.POST:
			return HttpResponseRedirect(reverse('cf:policies'))
		else:
			return super().post(request, *args, **kwargs)

class DomainUpdate(PermissionRequiredMixin, UpdateView):
	permission_required = 'controlframework.change_domain'
	model = Domain
	form_class = DomainForm
	template_name = 'controlframework/form.html'
	nav_item_active = navigation.app.nav_items['policies']

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context['title'] = 'Update Domain'
		context['nav_item_active'] = self.nav_item_active
		return context

	def post(self, request, *args, **kwargs):
		if "cancel" in request.POST:
			return HttpResponseRedirect(reverse('cf:policies/domain', kwargs={'pk':kwargs['pk']}))
		else:
			return super().post(request, *args, **kwargs)

class DomainDelete(PermissionRequiredMixin, DeleteView):
	permission_required = 'controlframework.delete_domain'
	model = Domain
	success_url = reverse_lazy('cf:policies')
	template_name='controlframework/form_confirm_delete.html'
	nav_item_active = navigation.app.nav_items['policies']

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context['title'] = 'Delete Domain'
		context['nav_item_active'] = self.nav_item_active
		return context

	def post(self, request, *args, **kwargs):
		print(kwargs)
		if "cancel" in request.POST:
			return HttpResponseRedirect(reverse('cf:policies/domain', kwargs={'pk':kwargs['pk']}))
		else:
			return super().post(request, *args, **kwargs)

## Generic Policy

class GenericPolicyView(PermissionRequiredMixin, DetailView):
	permission_required = 'controlframework.view_genericpolicy'
	model = GenericPolicy
	template_name = 'controlframework/policies/genericpolicy.html'
	nav_item_active = navigation.app.nav_items['policies']

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context['title'] = 'Generic Policy'
		context['nav_item_active'] = self.nav_item_active
		return context

class GenericPolicyCreate(PermissionRequiredMixin, CreateView):
	permission_required = 'controlframework.view_genericpolicy'
	model = GenericPolicy
	form_class = GenericPolicyForm
	template_name='controlframework/form.html'
	nav_item_active = navigation.app.nav_items['policies']
	disabled_fields = ['domain']

	def dispatch(self, request, *args, **kwargs):
		self.domain = get_object_or_404(Domain, pk=kwargs['domain_id'])
		return super().dispatch(request, *args, **kwargs)

	def get_initial(self):
		return { 'domain': self.kwargs['domain_id'] }

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context['title'] = 'Create Generic Policy'
		context['nav_item_active'] = self.nav_item_active
		return context

	def post(self, request, *args, **kwargs):
		if "cancel" in request.POST:
			return HttpResponseRedirect(self.domain.get_absolute_url())
		else:
			return super().post(request, *args, **kwargs)

class GenericPolicyUpdate(PermissionRequiredMixin, UpdateView):
	permission_required = 'controlframework.update_genericpolicy'
	model = GenericPolicy
	form_class = GenericPolicyForm
	template_name = 'controlframework/form.html'
	nav_item_active = navigation.app.nav_items['policies']

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context['title'] = 'Update Generic Policy'
		context['nav_item_active'] = self.nav_item_active
		return context

	def post(self, request, *args, **kwargs):
		if "cancel" in request.POST:
			return HttpResponseRedirect(reverse('cf:policies/genericpolicy', kwargs={'pk':kwargs['pk']}))
		else:
			return super().post(request, *args, **kwargs)

class GenericPolicyDelete(PermissionRequiredMixin, DeleteView):
	permission_required = 'controlframework.delete_genericpolicy'
	model = GenericPolicy
	success_url = reverse_lazy('cf:policies')
	template_name='controlframework/form_confirm_delete.html'
	nav_item_active = navigation.app.nav_items['policies']

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context['title'] = 'Delete Generic Policy'
		context['nav_item_active'] = self.nav_item_active
		return context

	def post(self, request, *args, **kwargs):
		print(kwargs)
		if "cancel" in request.POST:
			return HttpResponseRedirect(reverse('cf:policies/genericpolicy', kwargs={'pk':kwargs['pk']}))
		else:
			return super().post(request, *args, **kwargs)

## Policy

class PolicyView(PermissionRequiredMixin, DetailView):
	permission_required = 'controlframework.view_policy'
	model = Policy
	template_name = 'controlframework/policies/policy.html'
	nav_item_active = navigation.app.nav_items['policies']

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context['title'] = 'Policy'
		context['nav_item_active'] = self.nav_item_active
		return context

class PolicyCreate(PermissionRequiredMixin, CreateView):
	permission_required = 'controlframework.add_policy'
	model = Policy
	form_class = PolicyForm
	template_name='controlframework/form.html'
	nav_item_active = navigation.app.nav_items['policies']
	disabled_fields = ['generic_policy']

	def dispatch(self, request, *args, **kwargs):
		self.genericpolicy = get_object_or_404(GenericPolicy, pk=kwargs['genericpolicy_id'])
		return super().dispatch(request, *args, **kwargs)

	def get_initial(self):
		return { 'generic_policy': self.kwargs['genericpolicy_id'] }

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context['title'] = 'Create Policy'
		context['nav_item_active'] = self.nav_item_active
		return context

	def post(self, request, *args, **kwargs):
		if "cancel" in request.POST:
			return HttpResponseRedirect(self.genericpolicy.get_absolute_url())
		else:
			return super().post(request, *args, **kwargs)

class PolicyUpdate(PermissionRequiredMixin, UpdateView):
	permission_required = 'controlframework.change_policy'
	model = Policy
	form_class = PolicyForm
	template_name='controlframework/form.html'
	nav_item_active = navigation.app.nav_items['policies']
	disabled_fields = ['generic_policy', 'generic_object']

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context['title'] = 'Edit Policy'
		context['nav_item_active'] = self.nav_item_active
		return context

	def post(self, request, *args, **kwargs):
		print(kwargs)
		if "cancel" in request.POST:
			return HttpResponseRedirect(reverse('cf:policies/policy', kwargs={'pk':kwargs['pk']}))
		else:
			return super().post(request, *args, **kwargs)

class PolicyDelete(PermissionRequiredMixin, DeleteView):
	permission_required = 'controlframework.delete_policy'
	model = Policy
	success_url = reverse_lazy('cf:policies') #This has to be replaced with url of generic policy
	template_name='controlframework/form_confirm_delete.html'
	nav_item_active = navigation.app.nav_items['policies']

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context['title'] = 'Delete Policy'
		context['nav_item_active'] = self.nav_item_active
		return context

	def post(self, request, *args, **kwargs):
		print(kwargs)
		if "cancel" in request.POST:
			return HttpResponseRedirect(reverse('cf:policies/policy', kwargs={'pk':kwargs['pk']}))
		else:
			return super().post(request, *args, **kwargs)

