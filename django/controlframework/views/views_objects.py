from django.contrib.auth.mixins import PermissionRequiredMixin
from django.forms import ModelForm
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse, reverse_lazy
from django.views.generic import ListView, DetailView

from cdash_project.views import CreateView, UpdateView, DeleteView
from ..models import GenericObject, Object 
from ..forms import GenericObjectForm, ObjectForm
from .. import navigation

## Generic Object

class GenericObjectListView(PermissionRequiredMixin, ListView):
	permission_required = 'controlframework.view_genericobject'
	model = GenericObject
	template_name = 'controlframework/objects/objects.html'
	nav_item_active = navigation.app.nav_items['objects']

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context['title'] = 'Objects'
		context['nav_item_active'] = self.nav_item_active
		return context

class GenericObjectView(PermissionRequiredMixin, DetailView):
	permission_required = 'controlframework.view_genericobject'
	model = GenericObject
	template_name = 'controlframework/objects/genericobject.html'
	nav_item_active = navigation.app.nav_items['objects']

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context['title'] = 'Generic Object'
		context['nav_objects'] = 'active'
		context['nav_item_active'] = self.nav_item_active
		return context

class GenericObjectCreate(PermissionRequiredMixin, CreateView):
	permission_required = 'controlframework.add_genericobject'
	model = GenericObject
	form_class = GenericObjectForm
	template_name='controlframework/form.html'
	nav_item_active = navigation.app.nav_items['objects']

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context['title'] = 'Create Generic Object'
		context['nav_item_active'] = self.nav_item_active
		return context

	def post(self, request, *args, **kwargs):
		if "cancel" in request.POST:
			return HttpResponseRedirect(reverse('cf:objects'))
		else:
			return super().post(request, *args, **kwargs)

class GenericObjectUpdate(PermissionRequiredMixin, UpdateView):
	permission_required = 'controlframework.change_genericobject'
	model = GenericObject
	form_class = GenericObjectForm
	template_name='controlframework/form.html'
	nav_item_active = navigation.app.nav_items['objects']

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context['title'] = 'Edit Generic Object'
		context['nav_item_active'] = self.nav_item_active
		return context

	def post(self, request, *args, **kwargs):
		print(kwargs)
		if "cancel" in request.POST:
			return HttpResponseRedirect(reverse('cf:objects/genericobject', kwargs={'pk':kwargs['pk']}))
		else:
			return super().post(request, *args, **kwargs)

class GenericObjectDelete(PermissionRequiredMixin, DeleteView):
	permission_required = 'controlframework.delete_genericobject'
	model = GenericObject
	success_url = reverse_lazy('cf:objects')
	template_name='controlframework/form_confirm_delete.html'
	nav_item_active = navigation.app.nav_items['objects']

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context['title'] = 'Delete Generic Object'
		context['nav_item_active'] = self.nav_item_active
		return context

	def post(self, request, *args, **kwargs):
		print(kwargs)
		if "cancel" in request.POST:
			return HttpResponseRedirect(reverse('cf:objects/genericobject', kwargs={'pk':kwargs['pk']}))
		else:
			return super().post(request, *args, **kwargs)

## Object

class ObjectView(PermissionRequiredMixin, DetailView):
	permission_required = 'controlframework.view_object'
	model = Object
	template_name = 'controlframework/objects/object.html'
	nav_item_active = navigation.app.nav_items['objects']

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context['title'] = 'Object'
		context['nav_item_active'] = self.nav_item_active
		return context

class ObjectCreate(PermissionRequiredMixin, CreateView):
	permission_required = 'controlframework.add_object'
	model = Object
	form_class = ObjectForm
	template_name='controlframework/form.html'
	nav_item_active = navigation.app.nav_items['objects']
	disabled_fields = ['generic_object']

	def dispatch(self, request, *args, **kwargs):
		self.genericobject = get_object_or_404(GenericObject, pk=kwargs['genericobject_id'])
		return super().dispatch(request, *args, **kwargs)
	"""
	def get_form_kwargs(self, *args, **kwargs):
		kwargs = super(ObjectCreate, self).get_form_kwargs()
		kwargs['disabled_fields'] = ['generic_object']
		return kwargs
	"""

	def get_initial(self):
		genericobject = self.genericobject
		return { 'generic_object': self.kwargs['genericobject_id'] }

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context['title'] = 'Create Object'
		context['nav_item_active'] = self.nav_item_active
		return context

	def post(self, request, *args, **kwargs):
		if "cancel" in request.POST:
			return HttpResponseRedirect(self.genericobject.get_absolute_url())
		else:
			return super().post(request, *args, **kwargs)

class ObjectUpdate(PermissionRequiredMixin, UpdateView):
	permission_required = 'controlframework.change_object'
	model = Object
	form_class = GenericObjectForm
	template_name='controlframework/form.html'
	nav_item_active = navigation.app.nav_items['objects']

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context['title'] = 'Edit Object'
		context['nav_item_active'] = self.nav_item_active
		return context

	def post(self, request, *args, **kwargs):
		print(kwargs)
		if "cancel" in request.POST:
			return HttpResponseRedirect(reverse('cf:objects/object', kwargs={'pk':kwargs['pk']}))
		else:
			return super().post(request, *args, **kwargs)

class ObjectDelete(PermissionRequiredMixin, DeleteView):
	permission_required = 'controlframework.delete_object'
	model = Object
	success_url = reverse_lazy('cf:objects')
	template_name='controlframework/form_confirm_delete.html'
	nav_item_active = navigation.app.nav_items['objects']

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context['title'] = 'Delete Object'
		context['nav_item_active'] = self.nav_item_active
		return context

	def post(self, request, *args, **kwargs):
		print(kwargs)
		if "cancel" in request.POST:
			return HttpResponseRedirect(reverse('cf:objects/object', kwargs={'pk':kwargs['pk']}))
		else:
			return super().post(request, *args, **kwargs)
