from django.contrib.auth.mixins import PermissionRequiredMixin
from django.shortcuts import render 
from django.views.generic import ListView

from ..models import Control
from .. import navigation

def dashboard(request):
	return render(request, 'controlframework/dashboard.html',{'title': 'dashboard', 'nav_dashboard': 'active'})

## Control Framework

class ControlFrameworkListView(PermissionRequiredMixin, ListView):
	permission_required = 'controlframework.view_control'
	model = Control
	template_name = 'controlframework/controlframework/controlframework.html'
	nav_item_active = navigation.app.nav_items['controlframework']

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context['title'] = 'Control Framework'
		context['nav_item_active'] = self.nav_item_active
		return context



