from django.contrib.auth.mixins import PermissionRequiredMixin
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse, reverse_lazy
from django.views.generic import ListView, DetailView

from cdash_project.views import CreateView, UpdateView, DeleteView, ObjectDetailView
from ..models import GenericControl, Control
from ..forms import GenericControlForm, ControlForm
from .. import navigation

# Generic Control


class GenericControlListView(PermissionRequiredMixin, ListView):
	permission_required = 'controlframework.view_genericcontrol'
	model = GenericControl
	template_name = 'controlframework/controls/controls.html'
	nav_item_active = navigation.app.nav_items['controls']

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context['title'] = 'Controls'
		context['nav_item_active'] = self.nav_item_active
		return context

class GenericControlView(PermissionRequiredMixin, DetailView):
	permission_required = 'controlframework.view_genericcontrol'
	model = GenericControl
	template_name = 'controlframework/controls/genericcontrol.html'
	nav_item_active = navigation.app.nav_items['controls']

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context['title'] = 'Generic Control'
		context['nav_item_active'] = self.nav_item_active
		return context

class GenericControlCreate(PermissionRequiredMixin, CreateView):
	permission_required = 'controlframework.add_genericcontrol'
	model = GenericControl
	form_class = GenericControlForm
	template_name='controlframework/form.html'
	nav_item_active = navigation.app.nav_items['controls']

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context['title'] = 'Create Generic Control'
		context['nav_item_active'] = self.nav_item_active
		return context

	def post(self, request, *args, **kwargs):
		if "cancel" in request.POST:
			return HttpResponseRedirect(reverse('cf:controls'))
		else:
			return super().post(request, *args, **kwargs)

class GenericControlUpdate(PermissionRequiredMixin, UpdateView):
	permission_required = 'controlframework.change_genericcontrol'
	model = GenericControl
	form_class = GenericControlForm
	template_name='controlframework/form.html'
	nav_item_active = navigation.app.nav_items['controls']

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context['title'] = 'Edit Generic Control'
		context['nav_item_active'] = self.nav_item_active
		return context

	def post(self, request, *args, **kwargs):
		print(kwargs)
		if "cancel" in request.POST:
			return HttpResponseRedirect(reverse('cf:controls/genericcontrol', kwargs={'pk':kwargs['pk']}))
		else:
			return super().post(request, *args, **kwargs)

class GenericControlDelete(PermissionRequiredMixin, DeleteView):
	permission_required = 'controlframework.delete_genericcontrol'
	model = GenericControl
	success_url = reverse_lazy('cf:controls')
	template_name='controlframework/form_confirm_delete.html'
	nav_item_active = navigation.app.nav_items['controls']

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context['title'] = 'Delete Generic Control'
		context['nav_item_active'] = self.nav_item_active
		return context

	def post(self, request, *args, **kwargs):
		print(kwargs)
		if "cancel" in request.POST:
			return HttpResponseRedirect(reverse('cf:controls/genericcontrol', kwargs={'pk':kwargs['pk']}))
		else:
			return super().post(request, *args, **kwargs)

# Control

class ControlView(ObjectDetailView):
	permission_required = 'controlframework.view_control'
	model = Control
	template_name = 'hsplit_detail_tabs.html'
	templates = {
		'detail_template': 'controlframework/controls/control_detail.html',
		'tabs_template': 'controlframework/controls/control_tabs.html'
	}
	nav_item_active = navigation.app.nav_items['controls']

class ControlCreate(PermissionRequiredMixin, CreateView):
	permission_required = 'controlframework.add_control'
	model = Control
	form_class = ControlForm
	template_name='controlframework/form.html'
	nav_item_active = navigation.app.nav_items['controls']
	disabled_fields = ['generic_control']

	def dispatch(self, request, *args, **kwargs):
		self.genericcontrol = get_object_or_404(GenericControl, pk=kwargs['genericcontrol_id'])
		return super().dispatch(request, *args, **kwargs)

	def get_initial(self):
		return { 'generic_control': self.kwargs['genericcontrol_id'] }

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context['title'] = 'Create Control'
		context['nav_item_active'] = self.nav_item_active
		return context

	def post(self, request, *args, **kwargs):
		if "cancel" in request.POST:
			return HttpResponseRedirect(self.genericcontrol.get_absolute_url())
		else:
			return super().post(request, *args, **kwargs)

class ControlUpdate(PermissionRequiredMixin, UpdateView):
	permission_required = 'controlframework.change_control'
	model = Control
	form_class = ControlForm
	template_name='controlframework/form.html'
	nav_item_active = navigation.app.nav_items['controls']

	def get_form_kwargs(self, *args, **kwargs):
		kwargs = super().get_form_kwargs()
		kwargs['disabled_fields'] = ['generic_control', 'object']
		return kwargs

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context['title'] = 'Edit Control'
		context['nav_item_active'] = self.nav_item_active
		return context

	def post(self, request, *args, **kwargs):
		print(kwargs)
		if "cancel" in request.POST:
			return HttpResponseRedirect(reverse('cf:controls/control', kwargs={'pk':kwargs['pk']}))
		else:
			return super().post(request, *args, **kwargs)

class ControlDelete(PermissionRequiredMixin, DeleteView):
	permission_required = 'controlframework.delete_control'
	model = Control
	success_url = reverse_lazy('cf:controls')
	template_name='controlframework/form_confirm_delete.html'
	nav_item_active = navigation.app.nav_items['controls']

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context['title'] = 'Delete Control'
		context['nav_item_active'] = self.nav_item_active
		return context

	def post(self, request, *args, **kwargs):
		print(kwargs)
		if "cancel" in request.POST:
			return HttpResponseRedirect(reverse('cf:controls/control', kwargs={'pk':kwargs['pk']}))
		else:
			return super().post(request, *args, **kwargs)
