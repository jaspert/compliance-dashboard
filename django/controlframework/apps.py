from django.apps import AppConfig


class ControlframeworkConfig(AppConfig):
    name = 'controlframework'
