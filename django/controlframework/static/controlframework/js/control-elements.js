
(function($) { 

  $(document).ready(function(){
    $('a[data-toggle="tab"]').on('show.bs.tab', function(e) {
      localStorage.setItem('activeTab', $(e.target).attr('href'));
    });
    var activeTab = localStorage.getItem('activeTab');
    if(activeTab){
      $('#controlTab a[href="' + activeTab + '"]').tab('show');
    }
  });

  $(document).ready(function() {
    editForm(true);
  });

  $("#schedulerEdit").on('click', function(e) {
    editForm(false);
  });

  function editForm(toggle) {
    //$('#schedulerForm :input').prop('disabled', toggle);
    $('#schedulerForm :input').prop('readonly', toggle);
    $('#schedulerForm .form-check-input').prop('disabled', toggle);
    if (toggle) {
      $("#schedulerForm .btn").hide();
      $("#schedulerEdit").show()
    } else {
      $("#schedulerForm .btn").show();
      $("#schedulerEdit").hide()
    }
  }

  $('#inputDescription').on('input', function () { 
    this.style.height = 'auto'; 

    this.style.height =  
    (this.scrollHeight) + 'px'; 
  }); 

})(jQuery); // End of use strict
