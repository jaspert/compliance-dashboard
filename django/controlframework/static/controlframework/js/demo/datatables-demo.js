// Call the dataTables jQuery plugin
$(document).ready(function() {
	
	$('#controlframeworkTable').DataTable( {
		"order": [[ 0, "desc" ]],
		"dom": 
		"<'row justify-content-between'<'col-md-6'<'controltitle'>><'col-sm-12 col-md-6'f>>" 
		+ "<'row'<'col-sm-12'tr>>" 
		+ "<'row'<'col-sm-12 col-md-6'il><'col-sm-12 col-md-6'p>>",
		"language": {
			"infoEmpty": "0-0 of 0 entries",
			"info": "_START_-_END_ of _TOTAL_ entries",
			"lengthMenu": "_MENU_ entries / page",
		}
	});

	$("div.controltitle").html('<h6 class="m-0 font-weight-bold text-primary">Controls</h6>');
	
	$('#taskTable').DataTable( {
		"order": [[ 1, "desc" ]],
		"dom": 
		"<'row justify-content-between'<'col-md-6'<'tickettitle'>><'col-sm-12 col-md-6'f>>" 
		+ "<'row'<'col-sm-12'tr>>" 
		+ "<'row'<'col-sm-12 col-md-6'il><'col-sm-12 col-md-6'p>>",
		"language": {
			"infoEmpty": "0-0 of 0 entries",
			"info": "_START_-_END_ of _TOTAL_ entries",
			"lengthMenu": "_MENU_ entries / page",
		}
	});

	$('tr[data-href]').on("click", function() {
		document.location = $(this).data('href');
	});

	$('tr[data-href]').css('cursor','pointer');
});
