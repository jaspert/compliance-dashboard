from django.contrib import admin
from .models import Domain, GenericPolicy, Policy, GenericControl, Control, GenericObject, Object, Criticality, Frequency

admin.site.register(Domain)
admin.site.register(GenericPolicy)
admin.site.register(Policy)
admin.site.register(GenericObject)
admin.site.register(Object)
admin.site.register(GenericControl)
admin.site.register(Control)
admin.site.register(Criticality)
admin.site.register(Frequency)
