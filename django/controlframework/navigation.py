from django.urls import reverse_lazy
from cdash_project.navigation import App, NavItem

app = App(
	text = 'Framework',
	nav_items = {
		'controlframework': NavItem(
			text = 'Control Framework',
			url = reverse_lazy('cf:controlframework'),
			icon = 'fa-table',
			),
		'policies': NavItem(
			text = 'Policies',
			url = reverse_lazy('cf:policies'),
			icon = 'fa-file-alt',
			),
		'controls': NavItem(
			text = 'Controls',
			url = reverse_lazy('cf:controls'),
			icon = 'fa-check',
			),
		'objects': NavItem(
			text = 'Objects',
			url = reverse_lazy('cf:objects'),
			icon = 'fa-database',
			),
		}
	)