from django import forms
from cdash_project.forms import StandardForm
from .models import Domain, GenericPolicy, Policy, GenericObject, Object, GenericControl, Control


class DomainForm(StandardForm):

    class Meta:
        model = Domain
        fields = "__all__"

class GenericPolicyForm(StandardForm):

    class Meta:
        model = GenericPolicy
        fields = "__all__"

class PolicyForm(StandardForm):

    class Meta:
        model = Policy
        fields = "__all__"

class GenericObjectForm(StandardForm):

    class Meta:
        model = GenericObject
        fields = "__all__"

class ObjectForm(StandardForm):

    class Meta:
        model = Object
        fields = "__all__"

class GenericControlForm(StandardForm):

    class Meta:
        model = GenericControl
        fields = "__all__"

class ControlForm(StandardForm):

    class Meta:
        model = Control
        fields = "__all__"