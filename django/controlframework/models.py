from django.db import models
from django.urls import reverse
from django.contrib.auth.models import User
from django_celery_beat.models import CrontabSchedule

def get_name(self):
    return self.first_name+" "+self.last_name

User.add_to_class("__str__", get_name)


class Domain(models.Model):
    number = models.IntegerField(unique=True)
    name = models.CharField(max_length=200)
    description = models.TextField(blank=True)
    link = models.CharField(max_length=500, blank=True)

    def __str__(self):
        return str(self.number)+" "+self.name

    def get_absolute_url(self):
        return reverse('cf:policies/domain', kwargs={'pk':self.pk})

    class Meta:
        ordering = ['number']
        verbose_name = 'Domain'
        verbose_name_plural = 'Domains'

class GenericPolicy(models.Model):
    domain = models.ForeignKey(Domain, related_name='generic_policies', on_delete=models.PROTECT)
    number = models.IntegerField()
    name = models.CharField(max_length=200)
    description = models.TextField(blank=True)
    link = models.CharField(max_length=500, blank=True)

    def __str__(self):
        return str(self.domain.number)+"."+str(self.number)+" "+self.name

    def get_absolute_url(self):
        return reverse('cf:policies/genericpolicy', kwargs={'pk':self.pk})

    @property
    def parent(self):
        return self.domain

    class Meta:
        ordering = ['domain', 'number']
        unique_together = ('domain','number')
        verbose_name = 'Generic Policy'
        verbose_name_plural = 'Generic Policies'

class GenericObject(models.Model):
    name = models.CharField(max_length=200)
    description = models.TextField(blank=True)
    link = models.CharField(max_length=500, blank=True)
    #owner = models.ForeignKey(User, related_name='genericobjects', on_delete=models.PROTECT)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('cf:objects/genericobject', kwargs={'pk':self.pk})

    class Meta:
        ordering = ['name']
        verbose_name = 'Generic Object'
        verbose_name_plural = 'Generic Objects'

class Object(models.Model):
    generic_object = models.ForeignKey(GenericObject, related_name='objects', on_delete=models.CASCADE)
    name = models.CharField(max_length=200)
    description = models.TextField(blank=True)
    link = models.CharField(max_length=500, blank=True)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('cf:objects/object', kwargs={'pk':self.pk})

    @property
    def parent(self):
        return self.generic_object

    class Meta:
        ordering = ['generic_object', 'name']
        verbose_name = 'Object'
        verbose_name_plural = 'Objects'

class Policy(models.Model):
    generic_policy = models.ForeignKey(GenericPolicy, related_name='policies', on_delete=models.PROTECT)
    generic_object = models.ForeignKey(GenericObject, related_name ='policies', on_delete=models.PROTECT)
    owner = models.ForeignKey(User, related_name='owned_policies', on_delete=models.PROTECT)
    accountable = models.ForeignKey(User, related_name='accountable_policies', on_delete=models.PROTECT)
    narrative = models.URLField(max_length=300, blank=True)

    def __str__(self):
        return str(self.generic_policy)+" - "+str(self.generic_object)

    def get_absolute_url(self):
        return reverse('cf:policies/policy', kwargs={'pk':self.pk})

    @property
    def parent(self):
        return self.generic_policy

    class Meta:
        ordering = ['generic_policy', 'generic_object']
        unique_together = ('generic_policy','generic_object')
        verbose_name = 'Policy'
        verbose_name_plural = 'Policies'

class GenericControl(models.Model):
    generic_policy = models.ForeignKey(GenericPolicy, related_name='generic_controls', on_delete=models.PROTECT)
    name = models.CharField(max_length=200)
    description = models.TextField(blank=True)
    link = models.CharField(max_length=500, blank=True)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('cf:controls/genericcontrol', kwargs={'pk':self.pk})

    class Meta:
        ordering = ['name']
        verbose_name = 'Generic Control'
        verbose_name_plural = 'Generic Controls'

class Criticality(models.Model):
    value = models.CharField(max_length=200)

    def __str__(self):
        return self.value

    class Meta:
        verbose_name = 'Criticality'
        verbose_name_plural = 'Criticalities'

class Frequency(models.Model):
    number = models.IntegerField()
    value = models.CharField(max_length=200)
    duration = models.IntegerField()
    cron = models.ForeignKey(CrontabSchedule, on_delete=models.PROTECT, null=True) # to do: make non-nullable

    def __str__(self):
        return self.value

    '''def get_interval(self):
        interval = {
            'year': self.interval.get('year', 0),
            'month': self.interval.get('month', 0),
            'week': self.interval.get('week', 0),
            'day': self.interval.get('day', 0),
            'hour': self.interval.get('hour', 0),
            'minute': self.interval.get('minute', 0)
        }
        return interval'''

    class Meta:
        ordering = ['number']
        verbose_name = 'Frequency'
        verbose_name_plural = 'Frequencies'

class Control(models.Model):
    KEY = 1
    NON_KEY = 2
    NA = 3

    CRITICALITY_CHOICES = [
        (KEY, 'Key'),
        (NON_KEY, 'Non key'),
        (NA, 'N/A'),
    ]

    generic_control = models.ForeignKey(GenericControl, related_name='controls', on_delete=models.PROTECT)
    object = models.ForeignKey(Object, on_delete=models.PROTECT)
    owner = models.ForeignKey(User, related_name='controls', on_delete=models.PROTECT)
    sox = models.IntegerField(choices=CRITICALITY_CHOICES)
    pci = models.IntegerField(choices=CRITICALITY_CHOICES)
    frequency = models.ForeignKey(Frequency, related_name='controls', on_delete=models.PROTECT)
    custom_duration = models.IntegerField(blank=True, null=True)
    search_string = models.CharField(max_length=10000, blank=True)

    def __str__(self):
        return str(self.generic_control)+" - "+str(self.object)

    def get_absolute_url(self):
        return reverse('cf:controls/control', kwargs={'pk':self.pk})
    
    @property
    def parent(self):
        return self.generic_control

    @property
    def duration(self):
        if self.custom_duration is None:
            return self.frequency.duration
        else:
            return self.custom_duration

    @property
    def policy(self):
        return Policy.objects.filter(generic_policy=self.generic_control.generic_policy,generic_object=self.object.generic_object).first

    class Meta:
        ordering = ['generic_control', 'object']
        unique_together = ('generic_control','object')
        verbose_name = 'Control'
        verbose_name_plural = 'Controls'


