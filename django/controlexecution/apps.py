from django.apps import AppConfig
from django.conf import settings

class ControlexecutionConfig(AppConfig):
    name = 'controlexecution'

    def ready(self):
        # load the signals module
        import controlexecution.signals
