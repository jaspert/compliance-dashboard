from django.db import models
from django.urls import reverse
from datetime import datetime, date
from django_celery_beat.models import PeriodicTask, CrontabSchedule

from controlframework.models import Control

class ControlTask(models.Model):
    control = models.OneToOneField(Control, related_name="control_task", on_delete=models.CASCADE)

    issue_id = models.IntegerField(blank=True, null=True);
    issue_key = models.CharField(max_length=20, blank=True, null=True);

    def get_tasks_per_status(self):
        tasks = {}
        for calculated_status in Task.CALCULATED_STATUSES:
                nr_tasks = self.tasks.filter(calculated_status=calculated_status[0]).count()
                if ((not calculated_status[0] == Task.DONE and nr_tasks > 0)
                    or (not tasks and calculated_status[0] == Task.DONE)):
                        tasks[calculated_status[1]] = {
                            'nr_tasks': nr_tasks,
                            'label_color': Task.LABEL_COLORMAP[calculated_status[0]],
                        }
        return tasks

    def __str__(self):
        return str("Control task for {}".format(self.control))

    class Meta:
            verbose_name = 'Control Task'
            verbose_name_plural = 'Control Tasks'

class ScheduledTask(models.Model):
    enabled = models.BooleanField(default = True)
    initial_run = models.BooleanField(default = False)

    control_task = models.OneToOneField(ControlTask, related_name="scheduled_task", on_delete=models.CASCADE)

    description = models.TextField(max_length=5000, blank=True, null=True)
    assignee = models.CharField(max_length=254, blank=True, null=True)
    watchers = models.CharField(max_length=500, blank=True, null=True)
    start_date = models.DateTimeField(blank=True, null=True)
    end_date = models.DateTimeField(blank=True, null=True)
    custom_cron = models.ForeignKey(CrontabSchedule, on_delete=models.PROTECT, blank=True, null=True)

    celery_periodic_task = models.OneToOneField(PeriodicTask, related_name='scheduled_task', on_delete=models.CASCADE, blank=True, null=True)

    @property
    def summary(self):
        summary = f"{self.control_task.control} | {datetime.now().strftime('%Y-%m-%d')}"
        return summary

    @property
    def cron(self):
        if self.custom_cron:
            return self.custom_cron
        else:
            return self.control_task.control.frequency.cron

    def __str__(self):
        return str("Scheduled task for {}".format(self.control_task.control))

    class Meta:
        verbose_name = 'Scheduled Task'
        verbose_name_plural = 'Scheduled Tasks'

class Task(models.Model):
    OVERDUE = 2
    NEARLY_DUE = 3
    TO_DO = 4
    DONE = 1

    CALCULATED_STATUSES = (
        (OVERDUE, 'Overdue'),
        (NEARLY_DUE, 'Nearly Due'),
        (TO_DO, 'To Do'),
        (DONE, 'Done'),
    )

    JIRA_STATUSES = {
        TO_DO: 'To Do',
        DONE: 'Done',
    }

    LABEL_COLORMAP = {
        OVERDUE: 'danger',
        NEARLY_DUE: 'warning',
        TO_DO: 'primary',
        DONE: 'success',
    }

    control_task = models.ForeignKey(ControlTask, related_name='tasks', on_delete=models.CASCADE, null=True)

    issue_id = models.IntegerField(unique=True)
    issue_key = models.CharField(max_length=20)
    issue_type = models.CharField(max_length=254, blank=True, null=True)

    summary = models.CharField(max_length=254, blank=True, null=True)
    description = models.TextField(blank=True, null=True)

    assignee_username = models.CharField(max_length=254, blank=True, null=True)
    assignee_displayname = models.CharField(max_length=254, blank=True, null=True)

    compliance_id = models.CharField(max_length=254, blank=True, null=True)

    status_category = models.CharField(max_length=254, blank=True, null=True)
    status = models.CharField(max_length=254, blank=True, null=True)
    created_date = models.DateTimeField(auto_now=False, auto_now_add=False)
    due_date = models.DateField(auto_now=False, auto_now_add=False, blank=True, null=True)
    resolution_date = models.CharField(max_length=254, blank=True, null=True)

    calculated_status = models.IntegerField(choices=CALCULATED_STATUSES, default = TO_DO)

    def save(self, *args, **kwargs):
        self.calculated_status = self.calculate_status()
        super(Task, self).save(*args, **kwargs)

    def calculate_status(self):
        if self.status_category == self.JIRA_STATUSES[self.DONE]:
            calculated_status = self.DONE
        elif self.due_date and self.due_date < date.today():
            calculated_status = self.OVERDUE
        elif self.due_date and self.is_nearly_due():
            calculated_status = self.NEARLY_DUE
        else:
            calculated_status = self.TO_DO
        return calculated_status
    
    def get_absolute_url(self):
        return reverse('ce:tasks/task', kwargs={'pk':self.pk})

    def is_nearly_due(self):
        #todo: implement lookup table for days left per frequency
        if (self.get_days_left() < 10):
            return True
        else:
            return False

    def get_calculated_status_label_color(self):
        return self.LABEL_COLORMAP[self.calculated_status]

    def get_days_left(self):
        if not self.status_category == self.JIRA_STATUSES[self.DONE] and self.due_date:
            return (self.due_date - date.today()).days
        else:
            return ''

    def get_created_date(self):
        return self.created_date.strftime('%Y-%m-%d')

    def get_due_date(self):
        if self.due_date:
            return self.due_date.strftime('%Y-%m-%d')

    def update_statuses():
        for task in Task.objects.exclude(calculated_status=Task.DONE):
            task.save()

    def __str__(self):
        return str(self.issue_key)

    class Meta:
        ordering = ['-created_date']
        verbose_name = 'Task'
        verbose_name_plural = 'Tasks'