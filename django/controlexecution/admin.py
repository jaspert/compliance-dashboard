from django.contrib import admin
from .models import Task, ControlTask, ScheduledTask

# Register your models here.
admin.site.register(Task)
admin.site.register(ControlTask)	
admin.site.register(ScheduledTask)	
