from cdash_project.navigation import App, NavItem

app = App(
	text = 'Control Execution',
	nav_items = {
		'tasks' : NavItem(
			text = 'Tasks',
			url = '#',
			icon = 'fa-check-square',
			),
		'scheduler': NavItem(
			text = 'Scheduler',
			url = '#',
			icon = 'fa-calendar',
			),
		'notifications': NavItem(
			text = 'Notifications',
			url = '#',
			icon = 'fa-envelope',
			children = {
				'emails': NavItem(
					text = 'Emails',
					url = '#',
					),
				'push_messages': NavItem(
					text = 'Push Messages',
					url = '#',
					),
				},
			),
		},
	)


