import json
from django_celery_beat.models import PeriodicTask
from .models import Task, ScheduledTask
from controlframework.models import Control
from .tasks import create_control_issue, create_jira_subtask, update_jira_subtask_duedate

from django.db.models.signals import post_save, post_delete
from django.dispatch import receiver

@receiver(post_save, sender=Task)
def task_is_created(sender, instance, created, **kwargs):
    if not instance.due_date:
        update_jira_subtask_duedate.delay(instance.id)

@receiver(post_save, sender=Control)
def control_is_created(sender, instance, created, **kwargs):
    if created:
        create_control_issue.delay(instance.id)

@receiver(post_save, sender=ScheduledTask)
def scheduled_task_is_created_or_updated(sender, instance, created, **kwargs):
    if not instance.celery_periodic_task:
        instance.celery_periodic_task = PeriodicTask.objects.create(
            crontab = instance.cron,
            name = f"{instance}",
            task = 'controlexecution.tasks.create_jira_subtask',
            args = json.dumps([instance.id]),
            enabled = instance.enabled
            # to do: extend fields
        )
        instance.save()
    else:
        instance.celery_periodic_task.crontab = instance.cron
        instance.celery_periodic_task.name = f"{instance}"
        instance.celery_periodic_task.args = json.dumps([instance.id])
        instance.celery_periodic_task.enabled = instance.enabled
        instance.celery_periodic_task.save()
    if instance.initial_run:
        create_jira_subtask.delay(instance.id)
        instance.initial_run = False
        instance.save()

@receiver(post_delete, sender=ScheduledTask)
def auto_delete_celery_periodic_task(sender, instance, **kwargs):
    instance.celery_periodic_task.delete()