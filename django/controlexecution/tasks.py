from celery import shared_task
from datetime import datetime, timedelta
from dateutil import parser
from jira import JIRA
from time import sleep
from typing import Dict

from django.core.exceptions import ObjectDoesNotExist
from django.conf import settings

from controlframework.models import Control
from cdash_project.jira import JiraError
from .models import ControlTask, ScheduledTask, Task

@shared_task(bind=True)
def create_control_issue(self, control_id: int) -> None:
    
    # Load control data
    control = Control.objects.get(id=control_id)
    
    # Create issue in Jira
    try:
        jira = JIRA(settings.JIRA_API_URL,
            basic_auth = (settings.JIRA_SVC_USER, settings.JIRA_SVC_PASSWORD))
        issue_dict = {
            'project': {'key': settings.JIRA_PROJECT},
            'issuetype': {'name': 'Control'},
            'summary': f"{control}",
            'description': f"""
                This is the task for control [{control}|{settings.CDASH_URL}{control.get_absolute_url()}].\n
                Do not edit or close this task.\n\n
                Control tasks will be linked to this task."""
        }
        control_issue = jira.create_issue(fields=issue_dict)
        print(f"Created task for control {control}: {control_issue.key}")
    except Exception as exc:
        raise JiraError("An error occurred while creating a control issue in Jira.") from exc
    
    # Create controltask in database
    control_task = ControlTask.objects.create(
        control=control,
        issue_id=control_issue.id,
        issue_key=control_issue.key
    )
    print(f"Created controltask for jira issue {control_issue.key}: {control_task}")
    return 

@shared_task(bind=True)
def update_jira_subtask_duedate(self, task_id: int) -> None:

    # Load task data and calculate due date
    task = Task.objects.get(id=task_id)
    due_date = task.created_date.date() + timedelta(days=task.control_task.control.duration)
    
    # Update issue in Jira
    try:
        jira = JIRA(settings.JIRA_API_URL,
            basic_auth = (settings.JIRA_SVC_USER, settings.JIRA_SVC_PASSWORD))
        subtask = jira.issue(task.issue_id)
        subtask.update(duedate=due_date.strftime('%Y-%m-%d'))
        print(f"Due date updated for task {task}")
    except Exception as exc:
        raise JiraError("An error occurred while updating the due date of task {task.issue_key} in Jira.") from exc
    return 

@shared_task(bind=True)
def process_incoming_jira_subtask(self, request_body: Dict, request_meta: Dict) -> None:
    
    issue = request_body.get('issue')
    issue_fields = issue.get('fields')

    # Only proceed in case parent of subtask is a known control task
    try:
        control_task = ControlTask.objects.get(issue_id=issue_fields.get('parent',{}).get('id'))
    except ObjectDoesNotExist:
        return
    
    # Load existing task to be updated, else create new one
    try:
        task = Task.objects.get(issue_id=issue.get('id'))
    except ObjectDoesNotExist:
        task = Task()
    
    # Populate attributes and save
    task.control_task = control_task
    task.issue_id = issue.get('id')
    task.issue_key = issue.get('key')
    task.issue_type = issue.get('issue_type',{}).get('name')
    task.summary = issue_fields.get('summary')
    task.description = issue_fields.get('description')
    if issue_fields.get('assignee'):
        task.assignee_username = issue_fields.get('assignee').get('name')
        task.assignee_displayname = issue_fields.get('assignee').get('displayName')
    task.status_category = issue_fields.get('status',{}).get('statusCategory',{}).get('name')
    task.status = issue_fields.get('status',{}).get('name')
    task.created_date = parser.parse(issue_fields.get('created'))
    task.resolution_date = issue_fields.get('resolutiondate')
    if issue_fields.get('duedate'):
        task.due_date = datetime.strptime(issue_fields.get('duedate'), '%Y-%m-%d').date()
    task.save()

@shared_task(bind=True)
def process_deleted_jira_subtask(sef, request_body: Dict, request_meta: Dict) -> None:
    
    issue = request_body.get('issue')

    # If equivalent task present in compliance dashboard, delete it
    try:
        task = Task.objects.get(issue_id=issue.get('id'))
    except ObjectDoesNotExist:
        return
    task.delete()
    print(f"Jira issue {issue.get('id')} was deleted, deleted linked task in Compliance Dashbaord.")

@shared_task(bind=True)
def create_jira_subtask(self, scheduled_task_id: int) -> None:
   
    sleep(0.1)
    # Load scheduled task data
    scheduled_task = ScheduledTask.objects.get(id=scheduled_task_id)
    
    # Create issue in Jira
    try:
        jira = JIRA(settings.JIRA_API_URL,
            basic_auth = (settings.JIRA_SVC_USER, settings.JIRA_SVC_PASSWORD))
        issue_dict = {
            'project': {'key': settings.JIRA_PROJECT},
            'issuetype': {'name': 'Control Task'},
            'parent': {'id': str(scheduled_task.control_task.issue_id)},
            'summary': scheduled_task.summary,
            'description': scheduled_task.description,
        }
        task = jira.create_issue(fields=issue_dict)
        print(f"Created task for {scheduled_task}: {task.key}")
        task.update(assignee=scheduled_task.assignee)
    except Exception as exc:
        raise JiraError("An error occurred while creating a task in Jira.") from exc
    return 

@shared_task(bind=True)
def update_task_status(self) -> None:
    Task.update_statuses()
    return