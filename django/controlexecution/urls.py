from django.urls import path
from . import views

app_name = 'ce'
urlpatterns = [
	path('jira/subtask', views.jira_subtask, name='jira_subtask'),
]