from django.shortcuts import render

from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_POST

from cdash_project.jira import WebhookRequest
from .tasks import process_deleted_jira_subtask, process_incoming_jira_subtask

@csrf_exempt
@require_POST
def jira_subtask(request):
    webhook_req = WebhookRequest(request)
    
    if 'jira:issue' in webhook_req.body.get('webhookEvent') and 'parent' in webhook_req.body.get('issue',{}).get('fields'):
        if 'jira:issue_deleted' in webhook_req.body.get('webhookEvent'):
            process_deleted_jira_subtask.delay(webhook_req.body, webhook_req.meta) 
        else:
            process_incoming_jira_subtask.delay(webhook_req.body, webhook_req.meta)
    return HttpResponse(status=200)