import datetime
from django.conf import settings
from importlib import util, import_module

def get_current_year_to_context(request):
	current_datetime = datetime.datetime.now()
	return {
		'current_year': current_datetime.year
	}

def get_app_navigation_to_context(request):
	app_list = []
	for app in settings.APP_LIST:
		if util.find_spec(app+".navigation") is not None:
			navigation = import_module(app+".navigation")
			app_list.append(navigation.app)
	return {
		'app_list': app_list
	}

