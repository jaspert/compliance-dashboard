import copy, json

class JiraError(Exception):
    """Exception raised for failed interaction with Jira."""
    pass

class WebhookRequest:
    
    def __init__(self, request):
        self.body = json.loads(request.body.decode('utf-8'))
        meta = copy.copy(request.META)
        meta_cleaned = {}
        for k, v in meta.items():
            if isinstance(v, str):
                meta_cleaned[str(k)] = v
        self.meta = meta_cleaned

