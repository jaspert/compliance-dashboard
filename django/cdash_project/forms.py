from django.forms import ModelForm


class StandardForm(ModelForm):
	""" This class takes care of disabling fields in the form. """

	def __init__(self, *args, **kwargs):
		# Disable fields passed to this form
		disabled_fields = []
		try:
			disabled_fields = kwargs.pop('disabled_fields')
		except KeyError:
			pass
		super(StandardForm, self).__init__(*args, **kwargs)
		for field in disabled_fields:
			self.fields[field].disabled = True