class App():
	def __init__(self, text, nav_items=None):
		self.text = text
		self.nav_items = nav_items

class NavItem():
	def __init__(self, text, url, icon=None, children=None):
		self.text = text
		self.icon = icon
		self.url = url
		self.children = children