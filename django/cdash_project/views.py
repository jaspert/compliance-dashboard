from django.contrib.auth.mixins import PermissionRequiredMixin
from django.contrib.auth.models import Permission
from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView as Original_CreateView, UpdateView as Original_UpdateView, DeleteView as Original_DeleteView

class CreateView(Original_CreateView):
	disabled_fields = []

	def get_form_kwargs(self, *args, **kwargs):
		kwargs = super(CreateView, self).get_form_kwargs()
		kwargs['disabled_fields'] = self.disabled_fields
		return kwargs

class UpdateView(Original_UpdateView):
	disabled_fields = []

	def get_form_kwargs(self, *args, **kwargs):
		kwargs = super(UpdateView, self).get_form_kwargs()
		kwargs['disabled_fields'] = self.disabled_fields
		return kwargs

class DeleteView(Original_DeleteView):
    pass

class ObjectDetailView(PermissionRequiredMixin, DetailView):

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		# title
		context['title'] = context['object'].__str__
		# navigation, breadcrumbs
		context['nav_item_active'] = self.nav_item_active
		if hasattr(self, 'nav_item_child_active'):
			context['nav_item_child_active'] = self.nav_item_child_active
		if hasattr(self, 'templates'):
			context['templates'] = self.templates
		context['breadcrumbs'] = self.get_breadcrumbs(context['object'])
		context['model_permissions'] = self.get_model_permissions(context['object'], {'delete', 'change'})
		return context
	
	def get_breadcrumbs(self, object):
		breadcrumbs = []
		breadcrumbs.insert(0, self.nav_item_active)
		while hasattr(object, 'parent'):
			object = object.parent
			breadcrumbs.insert(1, {
				'text': object.__str__(),
				'url': object.get_absolute_url()
				})
		return breadcrumbs
	
	def get_model_permissions(self, object, actions):
		permissions = {}
		for action in actions:
			permissions[action] = (f"{object._meta.app_label}.{action}_{object._meta.model_name}")
		return permissions




