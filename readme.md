# Compliance Dashboard

<div align="center">
<img src="documentation/img/logo.png" alt="logo" width="200"/>
<a href="https://www.youtube.com/watch?v=T9ogpxuXJqk">
<img src="documentation/img/youtube.png" alt="dashboard">
</a>
</div>

[Link to Youtube video](https://www.youtube.com/watch?v=T9ogpxuXJqk)
## Introduction
For the CS50 final project I came up with an idea for an application based on my daily job. I work for a consulting firm in the domain of Technology and Risk. What we do for our clients is to leverage technology to ease or improve the process of Risk Management. This is also where this project comes in. It’s called the ‘Compliance Dashboard’. In a nutshell it helps to store, orchestrate and monitor activities that are performed by companies to ensure that they comply with regulations. 

What does this entail? Let’s try to break it down and give a brief introduction to the world of risk and compliance before I explain how the tool works.

## Risks, Compliance and Controls
Taking risks is inherent to doing business. For example, if Google is going to build and launch a new product, this requires an upfront investment without a 100% guarantee that the investment will actually be recouped. Taking this risk is not  a problem as long as it is identified and managed. Taking too much risks can have disastrous effect on the longevity of the company, its stakeholders and its customers. Therefore companies might want to intrinsically identify, manage and reduce their risks. Google likely demands a solid business case to be written and approved before and investment is made to launch a new product. In some cases however, the regulator steps in to enforce certain risk reduction measures to protect customers, shareholders, employees or other stakeholders. For example, to protect customers from food poisoning they can put laws in place to enforce compliance with food safety standards and periodically audit the companies to which these apply.

Let’s explore how controls can help reduce risks by using an analogy. Imagine you’re going on a ski trip with friends by car. The mountain roads might be covered with snow and very slippery. There is a risk that your car might get stuck or slide off the road. To reduce this risk you’ve fitted your car with winter tires. On top of this the local road regulations mandate the use of snow chains for all users of the mountain road when it’s snowing. Therefore you decide to put some snow chains on as well. The intrinsic use of winter tires and the mandated snow chains are examples of controls: activities that reduce risk.

## Compliance Dashboard Data Architecture
Then where does the compliance dashboard come in? It currently offers two apps:
- Control Framework
- Control Execution

### Control Framework
<div align="center">
<img src="documentation/img/controlframework_architecture_examples.png" alt="Control framework data architecture" width="75%">
</div>

The control framework application consists of several elements. 

The library contains the set of generic policies and generic controls that are the same for the entire organization. For example, there might be a change management policy that specifies several controls that have to be performed:
- All changes are requested on the ticketing system
- All changes are tested
- All changes are peer reviewed and approved

There is a list of business processes and IT systems which we call objects. Similar objects are grouped into object types. For example, there might be a group of financial applications of which the payments processor is one.

Generic policies and generic controls can be selected from the library and applied to these objects. An applied generic policy is called a policy. For example, all financial applications should adhere to the change management policy. Therefore a policy ‘Change Management for Financial Applications’ is created. An applied generic control is called a control. Because generic control ‘CHM01: Peer Code Review’ is part of the change management policy and because object Payments Processor is part of the financial applications a control is created: ‘CHM01 - Payments Processor’.

### Control Execution
 
<div align="center">
<img src="documentation/img/controlexecution_architecture_examples.png" alt="Control execution data architecture" width="75%">
</div>

Once the control framework has been composed, the control execution application can be used to generate periodic tasks for each control. Tasks are stored in Jira. The Compliance Dashboard interfaces with Jira to create tasks and monitor their progress. For each control an associated ‘control task’ is created in Jira. This serves as a container for all ‘tasks’ that are created on a periodic basis. The template for the task and the schedule at which the task is created is stored on the ‘scheduled task’.

## Compliance Dashboard User Interface

The UI consists of a navigation bar on the left side that can be collapsed and expanded. There is a search bar on top along with user account details. The latter is still under development. Then the page currently displayed is at the center. The UI is completely responsive and changes based on the viewport of the user. Throughout the tool data objects can be added, remove or edited by using the blue buttons.

### Dashboard
This provides an overview of the current control execution status. The dashboard is still under development and will be extended with new functionalities.

<img src="documentation/img/dashboard.png" alt="dashboard">

### Policies
This is a list of all Domains and Generic Policies. You can open up the details of one of these items by clicking them:

<img src="documentation/img/controlframework_policies.png" alt="policies">  

This is an example of a Domain. Each domain consists of one or more Generic Policies:

<img src="documentation/img/controlframework_domain.png" alt="domain">

This is an example of a Generic Policy. The linked Generic Controls and Policies are provided:  

<img src="documentation/img/controlframework_generic_policy.png" alt="generic_policy">

### Objects

This is a list of Oject Types and Objects. These are business processes or IT systems to which contorls can be applied. You can open up the details of one of these items by clicking them:

<img src="documentation/img/controlframework_objects.png" alt="objects">

### Controls

This is a list of all Generic Controls and Controls. You can open up the details of one of these items by clicking them:

<img src="documentation/img/controlframework_controls.png" alt="controls">

The control Framework provides a tabular overview of all controls that are currently in place. Note the status of the control task completion on the right column. You can open up one of these controls by clicking them.

<img src="documentation/img/controlframework.png" alt="controlframework">

This is the page for a control. It includes information on ownership, linked policy, frequency, criticality on top. On the bottom of the page tabs are available per topic. We'll cover the Tasks and Scheduler tabs in the controlexecution paragraph below.

<img src="documentation/img/controlframework_control.png" alt="control">

### Control Execution

For each control a scheduled task can be defined. This is the template for the periodic creation of a task in Jira. 

<img src="documentation/img/controlexecution_scheduled_task.png" alt="scheduled task">

Please note the bottom part of the page. This is a list of tasks in jira for this control. The status (To Do, In Progress, Done, Overdue) is automatically calculated based on the ticket status and due date..

<img src="documentation/img/controlframework_control.png" alt="control">

This is a Jira Task which can be opened by clicking one of the tasks on the control page.

<img src="documentation/img/controlexecution_jira_task.png" alt="jira task">

### Admin Interface

Some functionality such as user administration or monitoring of celery task completation can only be performed via the admin interface. This is provided by Django out of the box.


<img src="documentation/img/admin_interface.png" alt="admin interface">

## Compliance Dashboard Technology Stack

To easily pack, ship and run the application I've used Docker to containerize all the application's components. The docker-compose.yml file is a blueprint for all the containers. The application consists of the following components:
- **Django**: A Python based  web framework that consists of models, templates and viewws.
- **PostgresQL**: A relational database, which is used by Django to store the application's data.
- **Celery and RabbitMQ**: Celery is an asynchronous task queue that uses RabbitMQ as the message broker. Celery is used by the application to process interactions with external APIs and to periodically schedule tasks. It consists of these containers:
    - **RabbitMQ**: message broker
    - **Celery**: task queue
    - **Celery worker**: task executer  
    - **Celery beat**: task scheduler
- **Jira**: Software developed by Atlassian that helps teams manage work. In this case it is used to store and track control execution tasks.

<div align="center">
<img src="documentation/img/docker.png" alt="logo" width="200"/><br><br>
<img src="documentation/img/django.png" alt="logo" width="200"/><br><br>
<img src="documentation/img/postgres.png" alt="logo" width="200" style="background-color:white;"/><br><br>
<img src="documentation/img/celery_crop.png" alt="logo" width="200"/><br><br>
<img src="documentation/img/rabbitmq.png" alt="dashboard" width="200"><br><br>
<img src="documentation/img/jira.png" alt="logo" width="200"/><br><br>
</div>

## Files

- Docker
    - ```docker-compose.yml``` blueprint for all the containers
    - ```Dockerfile``` build configuration for the django app
- Django
    - ```manage.py``` main entrypoint for interacting with Django on the back-end
    - ```cdash_project``` core application, contains settings, reused elements like core tempaltes and views
    - ```controlframework``` application that contains policies, objects and controls 
        - ```models``` data models for domain, generic policy, policy, generic object, object, generic control, control
        - ```views``` processing of data that is presented to user in template
        - ```templates``` presentation layer for end user
        - ```navigation``` novigation elements loaded in to nav bar
        - ```urls``` mapping of url patterns to views
    - ```controlexecution``` application that containts tasks and interaction with Jira
        - ```models``` data models for control task, task, scheduled task
        - ```views``` processing of data that is presented to user in template
        - ```templates``` presentation layer for end user
        - ```navigation``` novigation elements loaded in to nav bar
        - ```urls``` mapping of url patterns to views
        - ```tasks``` tasks for interacting with Jira that can be called by celery
- Postgres
    - ```backup.sh``` shell script to generate database backup (dump)
    - ```restore.sh``` and ```restore_container.sh``` shell scripts to restore db backups
- Python
    - ```requirements.txt``` pip dependencies for django app
- Documentation
    - ```readme.md``` what you're reading right now
    - ```documentation``` folder containing source data for readme
