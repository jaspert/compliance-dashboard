#!/bin/bash

set -o allexport; source .env; set +o allexport
docker run --name=postgres_restore -e POSTGRES_DB=$POSTGRES_DB -e POSTGRES_USER=$POSTGRES_USER -e POSTGRES_PASSWORD=$POSTGRES_PASSWORD -p 5442:5432 -d postgres
