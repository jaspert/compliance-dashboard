#!/bin/bash

# based on https://stackoverflow.com/questions/63934856/why-is-pg-restore-segfaulting-in-docker
set -o allexport; source .env; set +o allexport

if [ -z "$1" ] || [ -z "$2" ]; then
    echo "Two arguments are required: filepath containername"
    exit 1
fi

FILEPATH="$1"
CONTAINER="$2"
FILE="${FILEPATH##*/}"
CMD="POSTGRES_PASSWORD=$POSTGRES_PASSWORD pg_restore -Fc -U $POSTGRES_USER -c -d $POSTGRES_DB /tmp/$FILE"


if test -f "$FILEPATH"; then
    docker cp $FILEPATH $CONTAINER:/tmp/$FILE
    docker exec -ti $CONTAINER bash -c "$CMD"
else
    echo "Filename '$FILE' is not valid."
fi
