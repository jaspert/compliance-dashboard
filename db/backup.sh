#!/bin/bash

# based on https://stackoverflow.com/questions/63934856/why-is-pg-restore-segfaulting-in-docker
set -o allexport; source .env; set +o allexport

FILENAME="db_dump_$(date +'%Y-%m-%d_%H_%M_%S').dump"
docker exec -ti compliance-dashboard-db-1 bash -c "pg_dump -Fc '$POSTGRES_DB' -U '$POSTGRES_USER' > /tmp/$FILENAME"
docker cp compliance-dashboard-db-1:/tmp/$FILENAME backups/$FILENAME
ls -l backups/$FILENAME
